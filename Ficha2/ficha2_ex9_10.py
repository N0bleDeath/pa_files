class Pessoa:
    def __init__(self, nome, sobrenome):
        self.nome = nome
        self.sobrenome = sobrenome
        
    def getNome(self):
        return self.nome + " " + self.sobrenome


class Aluno(Pessoa):
    def __init__(self, nome, sobrenome, num_mecanografico):
        super().__init__(nome, sobrenome)
        self.num_mecanografico = num_mecanografico
    
    def getAluno(self):
        return self.getNome() + ", " + str(self.num_mecanografico)
    
    def __str__(self):
        return self.getAluno()
aluno = Aluno("João", "Silva", 1234)
print(aluno.getAluno())