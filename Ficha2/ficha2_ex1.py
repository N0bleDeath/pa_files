class Celsius:
    def __init__(self, valor=0):
        self.temperatura = valor
        
    def to_fahrenheit(self):
        return (self.temperatura * 1.8) + 32
    
temp_c = Celsius(30)
print(temp_c.to_fahrenheit())