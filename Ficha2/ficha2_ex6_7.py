from num2words import num2words
class Celsius:
    def __init__ (self, valor=0):
        if valor <-275.15:
            valor= -273.15
        self.__temperatura = valor

    def get_temperatura(self):
        return self.__temperatura

    def set_temperatura(self, valor):
        if valor < -273.15:
            valor = -273.15
        self.__temperatura= valor
    temperatura = property(get_temperatura, set_temperatura)

    def __get_grau_str(self):
        if self.temperatura == 1 or self.temperatura == -1:
            return 'grau'
        else:
            return 'graus'

    def extenso(self):
        t = num2words(self.__temperatura, lang='pt') +' ' + self.__get_grau_str()+ ' Celsius'
        return t[0].upper()+ t[1:]

def soma (lista):
    sum = Celsius (0)
    for t in lista:
        sum.temperatura += t.temperatura
    return sum

def media (lista):
    m=soma(lista)
    m.temperatura/= len(lista)
    return m
lista = [Celsius(-300), Celsius(140), Celsius(30)]
resultado = soma (lista) 
resultado2 = media (lista)

print(resultado.get_temperatura())
print(resultado2.get_temperatura())
print (Celsius(-1).extenso())
print (Celsius(0).extenso())
print (Celsius(1).extenso())
print (Celsius(2).extenso())